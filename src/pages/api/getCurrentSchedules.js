import moment from "moment";
import database from "./database";

const getCurrentSchedules = async (req, res) => {
  try {
    let currentDate = moment(new Date()).format("DD/MM/YYYY");

    const { rows } = await database.query(
      `SELECT * FROM schedule WHERE date = $1 AND reserved = false`,
      [currentDate],
    );

    res.status(200).send(rows);
  } catch (error) {
    res.status(500).send({ error: error.message });
  };
};

export default getCurrentSchedules;