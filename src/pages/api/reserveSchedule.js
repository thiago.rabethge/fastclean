import database from "./database";

const reserveSchedule = async (req, res) => {
  try {
    let scheduleId = req.body.scheduleId;
    let carModel = req.body.carModel;
    let service = req.body.service;

    const { rows } = await database.query(
      `
        UPDATE schedule 
        SET reserved = true, car_model = $1, wash_type_id = $2 
        WHERE schedule_id = $3
      `,
      [carModel, service, scheduleId]
    );

    res.status(200).send(rows);
  } catch (error) {
    res.status(500).send({ error: error.message });
  };
};

export default reserveSchedule;