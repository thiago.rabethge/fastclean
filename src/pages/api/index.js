const Index = async (req, res) => {
  try {
    res.status(200).send({ "message": "hello world!" });
  } catch (error) {
    res.status(500).send({ "error": error.message });
  };
};

export default Index;