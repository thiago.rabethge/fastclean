'use client'
import Navbar from "@/components/navbar/navbar";
import getServices from "@/requests/services/getServices";
import useServicesStore from "@/stores/servicesStore";
import 'bootstrap/dist/css/bootstrap.css';
import { useMemo } from "react";

const Services = () => {
  const { servicesList, setServicesList } = useServicesStore();

  const GetServices = () => {
    getServices()
      .then((response) => setServicesList(response.data));
  };

  useMemo(() => {
    GetServices();
  }, []);

  return (
    <div className="container">
      <Navbar />

      <div className="text-center mb-3">
        <h3>Nossos serviços</h3>
      </div>

      <div className="row">
        {servicesList.map((service) => {
          return (
            <div key={service.wash_id} className="col-6">
              <div className="card">
                <div className="card-body">
                  <span>
                    <h5 className="card-title">
                      {service.name}
                    </h5>
                    <p className="card-text">
                      {service.description}
                    </p>
                  </span>
                </div>
              </div>
            </div>
          )
        })}
      </div>
      
    </div>
  );
};

export default Services;