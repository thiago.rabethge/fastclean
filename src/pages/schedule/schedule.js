'use client'
import ReserveScheduleModal from "@/components/modals/reserveScheduleModal";
import Navbar from "@/components/navbar/navbar";
import getCurrentSchedules from "@/requests/schedules/getCurrentSchedules";
import getServices from "@/requests/services/getServices";
import useCurrentSchedulesStore from "@/stores/schedulesStore";
import { useMemo } from "react";
import useServicesStore from "../../stores/servicesStore";
import Head from "next/head";
import Script from "next/script";

const Schedule = () => {
  const { currentSchedulesList, setCurrentSchedulesList, setSelectedSchedule } = useCurrentSchedulesStore();
  const { setServicesList } = useServicesStore();

  const GetCurrentSchedules = () => {
    getCurrentSchedules()
      .then((response) => setCurrentSchedulesList(response.data));
  };

  const GetServices = () => {
    getServices()
      .then((response) => setServicesList(response.data));
  };

  useMemo(() => {
    GetCurrentSchedules();
    GetServices();
  }, []);

  return (
    <>
      <div className="container">
        <Navbar />

        <div className="table-responsive">
          <table className="table table-hover">
            <thead>
              <tr>
                <th>
                  Data
                </th>
                <th>
                  Hora
                </th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {currentSchedulesList.map((currentSchedule) => {
                return (
                  <tr key={currentSchedule.schedule_id}>
                    <td>
                      {currentSchedule.date}
                    </td>
                    <td>
                      {currentSchedule.time}
                    </td>
                    <td>
                      <button
                        type="button"
                        className="btn btn-primary btn-sm"
                        data-bs-toggle="modal"
                        data-bs-target="#ReserveScheduleModal"
                        onClick={() => setSelectedSchedule(currentSchedule)}
                      >
                        Agendar
                      </button>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>

        <ReserveScheduleModal id="ReserveScheduleModal" GetCurrentSchedules={GetCurrentSchedules} />
        <Script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"></Script>
      </div>
    </>
  );
};

export default Schedule;