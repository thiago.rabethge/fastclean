import React, { Fragment } from "react";
import Navbar from "@/components/navbar/navbar";
import Link from "next/link";

const Home = () => {
  return (
    <Fragment>
      <div style={{
        backgroundColor: "black",
        color: "white",
        textAlign: "center",
      }}>
        <Link href={"/services/services"} style={{
          textDecoration: "none",
          color: "white",
        }}>
          Confira nossos serviços
        </Link>
      </div>

      <div className="container">
        <Navbar />

        <h3 className="mt-4 text-center">
          FastClean, seu veículo em boas mãos
        </h3>

        <div className="text-center mt-4">
          <Link href={"/schedule/schedule"} className="btn btn-primary btn-sm">
            Marcar uma lavação
          </Link>
        </div>
      </div>
    </Fragment>
  );
};

export default Home;