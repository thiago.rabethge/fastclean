import { create } from 'zustand';

const useCurrentSchedulesStore = create((set) => ({
  currentSchedulesList: [],
  setCurrentSchedulesList: (newCurrentScheduleList) => set(() => ({ currentSchedulesList: newCurrentScheduleList })),

  selectedSchedule: {},
  setSelectedSchedule: (newSelectedSchedule) => set(() => ({ selectedSchedule: newSelectedSchedule })),
}))

export default useCurrentSchedulesStore;