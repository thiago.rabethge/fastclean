import api from "@/services/api";

const putCurrentSchedules = (scheduleId, carModel, service) => {
  return (
    api
      .post("/api/reserveSchedule", {
        scheduleId: scheduleId,
        carModel: carModel,
        service: service,
      })
      .then((response) => response)
      .catch((error) => console.error({ error: error.message }))
  );
};

export default putCurrentSchedules;