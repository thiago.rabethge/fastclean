import api from "@/services/api";

const getCurrentSchedules = () => {
  return (
    api
      .get("/api/getCurrentSchedules")
      .then((response) => response)
      .catch((error) => console.error({ error: error.message }))
  );
};

export default getCurrentSchedules;