import 'bootstrap/dist/css/bootstrap.css';
import Head from 'next/head';
import Link from 'next/link';

const Navbar = () => {
  return (
    <>
      <ul className="nav nav-tabs mt-3 mb-3">
        <li className="nav-item">
          <Link className="nav-link active" href="/">
            Sobre
          </Link>
        </li>

        <li className="nav-item">
          <Link className="nav-link" href="../services/services">
            Serviços
          </Link>
        </li>
        
        <li className="nav-item">
          <Link className="nav-link" href="../schedule/schedule">
            Agendar
          </Link>
        </li>
      </ul>
    </>
  );
};

export default Navbar;