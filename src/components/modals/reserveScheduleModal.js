import putCurrentSchedules from "@/requests/schedules/putCurrentSchedules";
import useCurrentSchedulesStore from "@/stores/schedulesStore";
import useServicesStore from "@/stores/servicesStore";
import Swal from "sweetalert2";

const ReserveScheduleModal = (props) => {
  const { servicesList } = useServicesStore();
  const { selectedSchedule } = useCurrentSchedulesStore();

  const handleSubmitReserveScheduleForm = (e) => {
    e.preventDefault();

    let carModel = document.querySelector("#carModel").value;
    let service = document.querySelector("#service").value;

    putCurrentSchedules(selectedSchedule.schedule_id, carModel, service)
      .then(() => {
        Swal.fire(
          'Agendamento',
          'Agendamento concluído com sucesso',
          'success'
        );
        props.GetCurrentSchedules();
      });
  };

  return (
    <div className="modal fade" id={props.id} data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h1 className="modal-title fs-5" id="staticBackdropLabel">Agendar lavagem</h1>
            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <form onSubmit={(e) => handleSubmitReserveScheduleForm(e)}>
            <div className="modal-body">
              <div className="mb-3">
                <input id="carModel" className="form-control" placeholder="Modelo de carro" />
              </div>

              <div className="mb-3">
                <select id="service" className="form-control">
                  {servicesList.map((service) => {
                    return (
                      <option key={service.wash_id} value={service.wash_id}>
                        {service.name}
                      </option>
                    )
                  })}
                </select>
              </div>
            </div>
            <div className="modal-footer justify-content-center">
              <button type="button" className="btn btn-light" data-bs-dismiss="modal">Close</button>
              <input type="submit" className="btn btn-success" data-bs-dismiss="modal" value="Confirmar" />
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default ReserveScheduleModal;